> styl veřejných a oficiálních mluvených projevů

### Funkce

- agitační
- apelativní (výzvová)
- informační
- sdělná

### Forma

- mluvená

### Cíl

- Přimět posluchače k přemýšlení a přijetí stejných postojů a názorů, vliv na rozum a city příjemce
- Tematická mnohostrannost (projevy politické, soudní, náboženské, slavnostní, pohřební…)

### Požadavky

- Pečlivě promyšlený, připravený
- Jasná, zřetelná a plynulá řeč
- Přesvědčovat, získávat posluchače
- Přímý kontakt s posluchači:
  - srozumitelnost
  - vhodnost vzhledem k situaci a posluchači
- Využívání zvukových prostředků:
  - zřetelná artikulace
  - spisovná výslovnost
  - vhodné tempo řeči
  - mimika
  - gesta
- Dodržování společenských norem:
  - nesmrkat
  - nepít
  - nekašlat
  - neodvádět pozornost

### Slohový postup

- charakterizační
- informační
- popisný
- vyprávěcí
- výkladový
- úvahový

### Slohové útvary

- Projev:
  - Velice formální
  - Směřován širokému publiku
- Proslov:
  - Projev menšího rozsahu
  - Menší počet posluchačů (většinou navzájem známých)
  - Menší míra formálnosti
- Přednáška
  - Mluvený projev naučného obsahu

### Jazyk

- citace
- obrazná pojmenování
- zvolací věty
- řečnické otázky

### Syntax

- otázky přímé, nepřímé i řečnické
- několikanásobné větné členy
