> styl hromadných sdělovacích prostředků

### Funkce

- **agitační**
- sdělná
- rekreační

### Forma

- mluvená i psaná

### Cíl

- neny

### Požadavky

- aktuálnost
- objektivita

### Slohový postup

- **informační**
- popisný
- úvahový
- výkladový

### Slohové útvary

- fejeton
- interview
- inzerát
- komentář
- recenze
- reklama
- reportáž
- sloupek
- zpráva
- úvodník

### Jazyk

- spisovný
- knižní
- publicismy
- obrazná pojmenování
- automatizace (často používané výrazy)
- aktualizace (opak automatizace)

### Syntax

- věty jednoduché
- souvětí
- větné ekvivalenty
- přímá řeč
