> styl běžné každodenní komunikace

### Funkce

- prostě sdělná

### Forma

- mluvená

### Cíl

- předávání běžných informací

### Požadavky

- neny

### Slohový postup

- charakterizační
- informační
- popisný
- vyprávěcí
- úvahový

### Slohové útvary

- SMS
- jednoduché vyprávění
- jednoduchý popis
- maily
- omluvy
- představování
- přivítání
- telefonní rozhovor

### Znaky

- vázanost na konkrétní situaci (rozhovor)
- přímý kontakt s posluchačem (dialog)
- spontánnost
- nepřipravenost
- emocionalita
- expresivnost
- neverbálnost (gesta, mimika)

### Jazyk

- hovorová čeština
- obecná čeština
- nářečí
- profesní mluva
- slang
- argot
- ustálené stereotypní obraty (Jak se máš?)
- časté oslovovací výrazy
- nadměrné užívání ukazovacích zájmen
- samostatné větné členy
- vsuvky
- elipsy
- anakoluty
- kontaminace
- kontaktové výrazy (poslyšte, viď, že jo, hele, koukni)
- expresivní prostředky
- parazitní slova
- nespisovná výslovnost
- nesprávné tvary (s těma nohama, malujou, žijou)
- univerbizace (spacák, očař)

### Syntax

- převážně věty jednoduché
- souvětí
- větné ekvivalenty
