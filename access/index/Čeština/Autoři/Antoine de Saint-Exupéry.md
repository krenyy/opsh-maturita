### O autorovi

- Francouz
- Letec, spisovatel, novinář, poet
- Narozen 29. června 1900 v Lyonu
- Studoval ve Švýcarsku a v Paříži
- Považován za filosofa a humanistu
- Sloužil jako pilot, což se promítlo v jeho tvorbě
- Zemřel 31. července 1944 při průzkumném letu nad Středozemním mořem nedaleko
  Korsiky

### Díla

- [[Citadela]]
- [[Dopis rukojmímu]]
- [[Kurýr na jih]]
- [[Letec]]
- [[Malý princ]]
- [[Noční let]]
- [[Válečný pilot]]
- [[Země lidí]]

### Současníci

- [[Erich Maria Remarque]]
- [[Ernest Hemingway]]
- [[Franz Kafka]]
- [[George Orwell]]
- [[Henri Barbusse]]
- [[Michail Šolochov]]
- [[Romain Rolland]]
