### O autorovi

- Prozaik, novinář
- Nositel Nobelovy ceny
- Začínal jako žurnalista, s psaním románů začal později
- Těžce raněn jako dobrovolník na italské frontě
- Vyznával statečnost

### Díla

- [[Fiesta]]
- [[Komu zvoní hrana]]
- [[Muži bez žen]]
- [[Odpolední smrt]]
- [[Sbohem, armádo]]
- [[Stařec a moře]]
- [[Za našich časů]]

### Současníci

- [[Francis Scott Fitzgerald]]
- [[John Steinbeck]]
- [[Mark Twain]]
- [[William Faulkner]]
