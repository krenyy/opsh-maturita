### O autorovi

- Prozaik, dramatik, překladatel
- Stěží vystudoval Gymnázium a odstěhoval se do Prahy
- Po nedokončeném studiu práv se plně věnoval literatuře a publicistice
- Přispíval do časopisů Lumír, Národní listy, Česká revue
- Prosazoval umění pravdivé, usilující o nápravu světa
- Díky nedostatku peněz byl nucen se vrátit na Moravu, s čímž se však nikdy
  nesmířil (myslel si, že jediné místo, kde může něco znamenat je Praha)
- Svůj život ukončil sebevraždou

### Díla

- [[Bavlnkova žena]]
- [[Maryša]]
- [[Rok na vsi]]
- [[Santa Lucia]]

### Současníci

- [[Alexandr Sergejevič Puškin]]
- [[Alois Jirásek]]
- [[Alois Mrštík]]
- [[Charles Dickens]]
- [[Karel Václav Rais]]
- [[Ladislav Stroupežnický]]
- [[Nikolaj Vasiljevič Gogol]]
