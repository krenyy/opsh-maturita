### O autorovi

- Americký prozaik, básník a dramatik arménského původu.
- Autor řady humorně i nostalgicky laděných, částečně autobiografických povídek
  a románů, prodchnutých optimistickým vypravěčstvím, laskavou naivitou a
  soucitem s člověkem.
- Byl silně ovlivněn zkušenostmi z trpkého mládí, prožitého v prostředí
  přistěhovalecké chudiny v kalifornském Fresnu.
- Prošel různými zaměstnáními (pracoval mj. jako poslíček, telegrafista a
  přednosta poštovního úřadu), od třicátých let se věnoval literatuře a divadlu.
- V roce 1940 odmítl přijmout Pulitzerovu cenu za své pětiaktové drama.
- Dvakrát se oženil se stejnou ženou, dvakrát se s ní rozvedl.
- Mnoho let žil střídavě v Paříži a v rodném Fresnu, kde také zemřel.

### Díla

- [[Odvážný mladý muž na létající hrazdě]]
- [[Trable s tygry]]
- [[Jmenuji se Aram]]
- [[Lidská komedie]]
- [[Dobrodružství Wesleye Jacksona]]
- [[Tracyho tygr]]
- [[Mami, mám Tě ráda]]
- [[Tati, tobě přeskočilo]]
- [[O neumírání]]
- [[Náhodná setkání]]
- [[Nekrology]]
- [[Srdce na vysočině]]
- [[Kluci a holky, když jsou spolu]]
- [[Zápisky o životě, smrti a útěku na Měsíc]]

### Současníci

- [[Vladimir Nabokov]]
- [[Paulo Coelho]]
- [[William Styron]]
- [[Umberto Eco]]
- [[Alberto Moravia]]
- [[John Ronald Reuel Tolkien]]
- [[John Irving]]
