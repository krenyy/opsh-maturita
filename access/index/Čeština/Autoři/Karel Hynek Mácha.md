> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/sIyMPEy__kg"></iframe>

### O autorovi

- čech
- narozen v Praha-Malá strana, Rakouské císařství
- básník, prozaik
- Vystudoval filozofii a práva v Praze
- S Eleonorou Šomkovou měl syna Ludvíka
- Zemřel v den své svatby
- Ostatky přeneseny z Litoměřic do Prahy na Vyšehrad
- Velmi rád chodil pěšky po českých hradech a zámcích

### Díla

- [[Bratři]]
- [[Cikáni]]
- [[Křivoklad]]
- [[Máj]]
- [[Márinka]]
- [[Večer na Bezdězu]]

### Současníci

- [[Josef Kajetán Tyl]]
- [[Karel Jaromír Erben]]
- [[Karel Sabina]]
- [[Viktor Hugo]]
