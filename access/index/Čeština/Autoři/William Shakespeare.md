> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/b7B2yvWlkr8"></iframe>

### O autorovi

- angličan
- narozen v Stratford-upon-Avonu, Warwickshire, Anglie
- dramatik, poet, herec
- pocházel z rodiny sedláků z hrabství Warwickshire
- v 18 letech se oženil s Anne Hathaway
- byl zklamán vývojem společnosti, psal proto tragédie
- psal také romance a sonety
- zemřel 23. dubna 1616 zřejmě na horečku, kterou si přivodil na večírku

### Díla

- [[Hamlet]]
- [[Komedie omylu]]
- [[Král Lear]]
- [[Macbeth]]
- [[Marná lásky snaha]]
- [[Othello]]
- [[Sen noci svatojánské]]
- [[Večer tříkrálový]]
- [[Zkrocení zlé ženy]]
- [[Romeo a Julie]]

### Současníci

- [[Edmund Spenser]]
- [[Geoffrey Chaucer]]
- [[Giovanni Boccaccio]]
