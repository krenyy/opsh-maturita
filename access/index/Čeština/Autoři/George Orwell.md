### O autorovi

- nic zatím

### Díla

- [[1984]]
- [[Barmské dny]]
- [[Farářova dcera]]
- [[Farma zvířat]]

### Současníci

- [[John Ronald Reuel Tolkien]]
- [[Clive Staples Lewis]]
- [[Karel Čapek]]
- [[Josef Čapek]]
- [[Ota Pavel]]
- [[Antoine de Saint-Exupéry]]
- [[John Steinbeck]]
- [[Ernest Hemingway]]
