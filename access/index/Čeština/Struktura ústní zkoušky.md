### Analýza uměleckého textu

- **1\. část**

  - zasazení výňatku do kontextu díla
  - téma a motivy
  - časoprostor
  - kompoziční výstavba
  - literární druh a žánr

- **2\. část**

  - vypravěč / lyrický subjekt
  - postava
  - vyprávěcí formy
  - typy promluv

- **3\. část**

  - jazykové prostředky
  - tropy
  - figury

### Literárně-historický kontext díla

- kontext autorovy tvorby
- literární / obecně kulturní kontext

### Analýza neuměleckého textu

- **1\. část**

  - souvislost mezi výňatky
  - komunikační situace (např. účel, adresát)
  - funkční styl
  - slohový postup
  - slohový útvar

- **2\. část**

  - kompoziční výstavba výňatku
  - jazykové prostředky a jejich funkce ve výňatku
