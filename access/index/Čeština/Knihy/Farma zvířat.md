> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/evQQ6_XyQnc"></iframe>

### Autor

- [[George Orwell]]

### Literární

- **druh:**
  - epika
- **forma:**
  - próza
- **žánr:**
  - antiutopický román
- **směr:**
  - anglický literatura 20. století s prvky scifi

### Obsah

- neny :(

### Členění

- 10 kapitol

### Kompozice

- chronologická

### Téma

- Kritika koministického režimu

### Motivy

- zrada
- popravy
- poslušnost
- zaslepenost
- strach
- režim
- "otroctví"

### Postavy

- **Napoleon**
  - Odvážný rybář s pevnou vůlí, samotář, bojuje až na pokraj svých sil, aby se
    uživil, introvert, prostý člověk, starý, silný, vytrvalý
- **Kuliš**
  - Obětavý, laskavý, nápomocný, má rád starce, pomáhá mu obdivuje ho
- **Pan Jones, Boxer (kůň), Pištík (prase - řečník), Starý Major (prase - návrh
  na revoluci), Benjamin (osel), Molina (klisna - po revoluci odchází na
  sousední farmu), Lupina (klisna), Stádo ovcí, Slepice, Psi (strážci
  Napoleona), Kočka, Havran**

### Časoprostor

- Na statku v Anglii
- asi 1. pol. 20. století (?)

### Vypravěcí způsoby

- er-forma

### Jazyk

- spisovný
- Odborné termíny (animalismus)
- Slova se socialistickým zabarvením
- Obrazná pojmenování (hyperbola, alegorie, metafora, přirovnání, PERSONIFIKACE)
- Přímá a nepřímá řeč

### Syntax

- Souvětí, jednoduché věty, větné ekvivalenty
- Všechny druhy vět podle mluvčího
