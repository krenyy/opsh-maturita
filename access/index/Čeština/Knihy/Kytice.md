> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/UDEQsXg5g-Y"></iframe>

### Autor

- [[Karel Jaromír Erben]]

### Literární

- **druh:**
  - lyricko-epický
- **forma:**
  - poezie
- **žánr:**
  - sbírka balad
- **směr:**
  - česká literature, 19. století (národní obrození)

### Obsah

- **Kytice**
- **Poklad**
  - Je velký pátek. Žena, která spěchá do kostela na mši se svým dítětem objeví
    skálu, kterou nikdy na této cestě neviděla. Je v ní ukryto obrovské množství
    zlata a bohatství. Vchází dovnitř. Je nadšená. Začíná si nabírat zlaťáků co
    unese. Byla zaslepena zlatem a stříbrem natolik, že zapoměla na své dítě,
    které si ve skále odložila. Se zlaťáky došla domů. Zde se však proměnily na
    hlínu a kamení. Uvědomí si, že ve skále zapoměla své dítě, tak upaluje
    zpátky. Skála ani dítě tam však už nejsou. Po roce se opět na Velký pátek
    vydává po stejné cestě, kde znova stojí ta skála. Tentokrát však vejde, a
    jediné co si odtud odnese je její dítě.
- **Svatební košile**
  - Dívka čeká na svého milého, který odešel do ciziny a stále se nevrací. Modlí
    se k panně Marii a prosí ať se její milý vrátí nebo ať se její život zkrátí.
    Náhle však někdo zaklepal na okno. Byl to on. Chtěl ať s ním jde na hřbitov.
    Její milý ji nese vzduchem a po cestě ji přesvědčuje ať se zbaví
    modlitebních knížek, růžence a křížku, které si s sebou vzala. Když je
    odhodila, duch jejího milého zesílil. Dostávají se na hřbitov (jeho
    "domov"). Dívce dochází, že je v nebezpečí a podaří se jí schovat do
    márnice. Začíná se usilovně modlit. Duch jejího milence se snaží dostat
    dovnitř. Nakonec ale začne svítat, dívka přežije a duch zmizí. Jen dívčina
    košile, kterou ušila je roztrhaná.
- **Polednice**
  - Matka vaří oběd svému dítěti. To ji ale neposlouchá a pořád pláče. Matka je
    z pláče tak naštvaná, že na dítě začne přivolávat polednici. Polednice se
    ale opravdu zjeví a matka začíná litovat svých činů. Jak se k ní polednice
    přibližuje, tak k sobě tiskne dítě. Ve svém obětí ho ale udusí a matka upadá
    do bezvědomí. Když odbijí hodiny, polednice zmizí. Otec přichází domů,
    vzkřísí manželku, ale zjišťuje, že dítě je mrtvé.
- **Zlatý kolovrat**
  - Pán zabloudí v lese na lovu a má žízeň. Najde chaloupku, kde bydlí Dornička
    a ta mu dá napít. Za odměnu jí slíbí manželství. Když žádá o Dorniččinu
    ruku, její nevlastní matka nejprve nabídne její nevlastní sestru. Když král
    odjede, matka a sestra odvedou Dorničku do lesa, kde ji zabijí, useknou jí
    ruce a nohy, vypíchnou oči. Sestra se stane královnou. V lese najde Dorničku
    stařec. Pošle "své pachole" pro Dorniččiny oči, ruce a nohy. Výměnou za to
    tam pachole nechává zlatý kolovrat, přeslici a kužel. Nevlastní sestra pro
    krále začne příst. Vtu ale kolovrat začne zpívat o Dorniččině smrti. Král
    matku a sestru potrestá stejným způsobem, jako oni potrestali Dorničku.
    Stařec složí Dorničku dohromady a oživí ji. Pak se stává královnou.
- **Štědrý den**
- **Holoubek**
- **Záhořovo lože**
- **Vodník**
  - Vodník sedí u jezera a plánuje svou svatbu při měsíčním svitu. Nastává
    pátek, dcera chce jít k jezeru, matka ji ale prosí aby nechodila, měla prý
    zlý sen. Dcera ale neposlouchá - jde k jezeru, kde se utopí. Stává se tak
    vodníkovou ženou. Uplyne nějaký čas, a dcera s vodníkem mají dítě. Prosí ho,
    ať ji pustí rozloučit se se svou matkou. Vodník jí to nakonec povolil. Když
    nastane čas na návrat, matka svou dceru nechce pustit. Vodník se naštve a
    zabíjí dítě.
- **Vrba**
- **Lilie**
- **Dceřina kletba**
- **Věštkyně**

### Členění

- 13 baladických skladeb, 10 z nich tvoří tematické dvojice, které stojí proti
  sobě (první proti poslední, druhá proti předposlední)
- Kytice a Věštkyně - vlastenectví, víra v budoucnost, národní smutek
- Poklad a Dceřina kletba - vztah matky a dítěte, provinění matky
- Svatební košile a Vrba - vztah mrtvý-živý
- Polednice a Vodník - pohádkové bytosti nepřátelské člověku
- Zlatý kolovrat a Záhořovo lože (pohádkový motiv)
- Lilie - nebyla v 1. vydání, přidána dodatečně
- Holoubek - zařazen uprostřed jako 13. skladba bez dvojice
- Štědrý den - nemá dvojici, původně měl protějšek mít

### Kompozice

- chronologická

### Téma

- Vina, trest, bezmoc

### Motivy

-

### Postavy

- **Polednice**
  - Bájné stvoření, zejvující se kolem poledne na poli. Trestá ty, co na poli v
    dobu oběda pracují a unáší děti. Je vysoká (2m), připomíná starou babičku. V
    baladě představuje sílu, se kterou se matka nemůže měřit.

---

- **Vodník**
  - Osamělý a nedůvěřivý. Kvůli pomstě je ochoten zabít vlastního syna. Žije v
    jezeře, kde chytá duše utopených.
- **Dcera**
  - Neposlušná, ignoruje přání své matky.
- **Matka**
  - Nechce se vzdát své dcery, kterou miluje.

---

- **Dornička**
  - Krásná, hodná, důvěřivá dívka.
- **Nevlastní sestra**
  - Za jakoukoli cenu se chce stát královou ženou. Je zlá.

### Časoprostor

- Česko - les, hřbitov, dům, jezero, rybník
- Není určeno kdy

### Vypravěcí způsoby

- er-forma

### Jazyk

-
