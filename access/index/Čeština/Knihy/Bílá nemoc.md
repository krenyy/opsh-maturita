> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/y9gwXi-tPpI"></iframe>

### Autor

- [[Karel Čapek]]

### Literární

- **druh:**
  - drama
- **žánr:**
  - tragédie
- **směr:**
  - demokratický proud, 1. polovina 20. století, meziválečná literatura

### Obsah

- V jisté, blíže neurčené zemi, se rozmohla epidemie tzv. bílé nemoci. Jde
  pravděpodobně o jakousi formu malomocenství, která se projevuje bílými
  skvrnami na kůži. Šíří se dotykem a ze začátku pouze mezi chudými a starými
  lidmi. Zemi ovládá diktátor Maršál, který se připravuje na výbojnou válku
  proti menšímu sousednímu státu. V tom mu pomáhá vlastník zbrojovek baron Krüg.
  Mladý doktor Galén vynalezl lék proti bílé nemoci, ale poskytuje ho pouze
  chudým lidem, lidem bez moci a vlivu. Těm vlivnějším Galén lék vydat odmítá.
  Když onemocní baron Krüg, zavolá Galéna, aby ho vyléčil. Ten mu dává
  podmínku - musí okamžitě zastavit zbrojení, jinak mu lék nebude vydán. Krüg
  žádá Maršála, aby uzavřel mír, ten však trvá na dalším zvýšení zbrojní výroby
  a Krüg ze zoufalství spáchá sebevraždu. Po jeho smrti začíná útok na sousední
  zemi. Maršál na sobě zpozoruje bílou skvrnu a vyzývá fanatiky k dalšímu
  výbojnému tažení. Maršálova dcera pozve Galéna, aby ho vyléčil, ten má ale
  stále stejnou podmínku - Maršál musí uzavřít mír. Teprve na naléhání své dcery
  a mladého Krüga, je ochoten uzavřít mír a splnit tak Galénovu podmínku pro
  poskytnutí léku. Dá zavolat Galéna, ale ten se před Maršálovým palácem střetne
  se skandujícím davem a protože se snaží odporovat jejich provolávání hesla:
  "Ať žije Maršál, ať žije válka!" je davem ubit a jeho léky rozdupány. Dav v tu
  chvíli netuší, že svým fanatismem prakticky zabili oslavovaného vůdce. Na
  závěr vypuká válka.

### Členění

- 3 dějství, 14 obrazů

### Kompozice

- chronologická

### Téma

- varování před fašismem

### Motivy

- smrt
- válka
- vražda

### Postavy

- **Postava**
  - asdf

### Časoprostor

-

### Vypravěcí způsoby

- er-forma

### Jazyk

- Spisovná čeština, latina - názvy nemocí
- Anglické a německé fráze
- Krátké scénické poznámky
- Psáno formou dialogů
- Není zde velké množství charakteristiky postav (známo z jejich promluv)
- Fantastický motiv
- Metafory
- Nářečí
- Divadelní hra (drama)
- Symbolika:
  - Maršálova země - nacistické německo
  - Malá sousední země - československo
  - Bílá nemoc - úpadek společnosti, manipulace s lidmi
  - Galénova smrt - potrestání Galéna za porušení Hippokratovy přísahy

### Syntax

-
