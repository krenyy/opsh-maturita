> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/eqILtkQ4seI"></iframe>

### Autor

- [[William Shakespeare]]

### Literární

- **druh:**
  - drama
- **forma:**
  - drama
- **žánr:**
  - anglická milostná tragédie
- **směr:**
  - anglická renesance

### Obsah

- Příběh knihy začíná ve Veroně. Mladý Romeo z rodiny Montekových se právě
  rozešel se svojí dívkou Rosalinou. Je nešťastný, protože ona dala přednost
  bohu před ním. Rozhodla se vstoupit do řádu sester. Romeo je zoufalý a aby mu
  jeho přítel Benvolio zvedl náladu, navrhne mu, aby šli na ples (maškarní bál).
  Ten je ale pořádaný rodinou Kapuletů. Kapuletové a Montekové spolu už dlouhou
  dobu soupeří (válčí). Nikdo už neví, čím ten spor vzniknul.
- Romeo jde s Benvoliem na ples a tam se seznámí s Julií. Zamilují se do sebe,
  ale ona je z rodu Kapuletů, a proto by měla být jejich láska zapovězená. Oni
  se však nevzdávají a i přes ten odvěký spor se spolu znovu setkají. Ještě ten
  den po plese vyzná Romeo Julii v zahradě Kapuletových lásku. A protože se
  velmi milují, rozhodnou se pro tajnou svatbu. Oddá je bratr Vavřinec. Je to
  Romeův přítel. Nikdo se o té svatbě nedozví (kromě Juliiny chůvy, která jim se
  svatbou pomáhá).
- V den jejich svatby se ale Romeo utká Tybaltem z rodu Kapuletů. V té době je
  už manžel Julie. Romeo Tybalta zabije a je za to vyhnán z Verony. Romeo odjede
  do Mantova a tam se má ukrýt, dokud se na jeho prohřešky nezapomene.
- Protože je Julie nešťastná, rozhodne se její otec, že ji provdá za Parise.
  Julie si ale Parise vzít nemůže. Je už manželkou Romea. Proto si jde pro radu
  k bratru Vavřincovi. Ten jí dá tekutinu, po které má na 42 hodin usnout a
  vypadat jako mrtvá. Julie to noc před svojí "svatbou" s Parisem vypije. Další
  den ji najdou "mrtvou". Uloží ji do rodinné hrobky Kapuletů.
- Po její "smrti" se rozjede Romeův sluha do Mantova. Když se Romeo dozví o
  smrti své milé, vydá se za ní. Napřed si ale u lékárníka koupí jed. Vloupá se
  za pomoci svého sluhy do hrobky. Poté vykáže sluhu pryč (pohrozí mu smrtí,
  když neodejde). Ve stejnou dobu je v hrobce i Paris. Romeo požádá Parise, ať
  odejde, ale ten odmítá. Proto se spolu utkají. Romeo zabije Parise a truchlí
  nad Julií. Poté vypije jed.
- Chvíli po tom, co Romeo zemře, se Julie probudí. V té samé chvíli přispěchá do
  hrobky i bratr Vavřinec (bratr Vavřinec poslal Romeovi dopis o tom, co s Julií
  chystají; ten se k Romeovi ale nedostal, proto se bratr Vavřinec vydal k
  hrobce sám). Bratr Vavřinec se snaží odvést Julii, protože k hrobce přicházejí
  lidé (Parisův sluha slyšel hluk boje a šel pro pomoc). Julie ale odmítá odejít
  a bratr Vavřinec uteče sám. Poté, co bratr uteče, se Julie snaží otrávit
  zbytky Romeova jedu. To se jí ale nedaří, proto vezme Romeovu dýku a probodne
  se.
- Když přijdou lidé do hrobky a spatří, co se stalo, zavolají Monteky a
  Kapulety. Na hřbitově najdou bratra Vavřince a ten jim vše vysvětlí. Kapuleti
  a Montekové se nakonec ve svém neštěstí usmíří. Ale svým dětem vrátit život
  nemůžou a toho odvěkého sporu litují.

### Členění

- prolog
- 5 dějství

### Kompozice

- chronologická

### Téma

- Nenávist mezi rody Monteků a Kapuletů
- Tragická láska mezi Romeem a Julii
- Boj mezi Tybaltem a Merkuciem
- Juliina svatba s Parisem

### Motivy

- láska
- vdavky
- válka dvou rodů
- spory

### Postavy

- **Romeo**
  - Chlapec z rodu Monteků; byl zamilovaný do Rosaliny, s tou se ale "rozešel";
    na plese rodu Kapuletů se zamiluje do Julie; romantik, schopný pro lásku
    udělat vše; upřímně milující Julii
- **Julie**
  - Mladá dívka z rodu Kapuletů; je jí 14 let; na plese její rodiny se seznámí s
    Romeem, který je ale z rodu Monteků; Julie je milá, sympatická, citlivá a
    krásná, mladičká, trochu naivní, chytrá, sličná, oddaná dívka
- **Vedlejší postavy**
  - Tybalt (Juliin bratr, rváč), Paris, Merkucio (výmluvný a vtipný, Romeův
    přítel), Benvolio, paní Monteková, paní Kapuletová, bratr Vavřinec,
    Baltazar, lékárník, Juliina chůva

### Časoprostor

- Italské město Verona, 16. století

### Vypravěcí způsoby

- er-forma

### Jazyk

- archaismy: "Ni to, ni ono..."
- básnická pojmenování: antiteze, epiteton, metafora, personifikace,
  eufernismus, oxymorón
- citově zbarvené výrazy
- eufenismy
- metafora: "Na křídlech lásky přeletěl jsem zeď..."
- přirovnání: "Tu se Romeo vymrštil jako střela."
- přímá řec - dialogy, monology
- spisovný jazyk
- veršovaná forma, jambický verš
- věty jednoduché, souvětí
- věty tázací, zvolací, jednočlenné, dvojčlenné
- řečnické otázky: "Ach Romeo, Romeo! Proč jsi Romeo?"
