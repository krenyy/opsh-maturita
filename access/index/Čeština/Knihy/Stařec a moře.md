> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/JpijWUFMbO4"></iframe>

### Autor

- [[Ernest Hemingway]]

### Literární

- **druh:**
  - epika
- **forma:**
  - próza
- **žánr:**
  - novela
- **směr:**
  - světová literatura 1. pol. 20. století

### Obsah

- Santiago, chudý samotář se silnou vůlí, který v posledních 84 dnech neulovil
  ani jednu pořádnou rybu, se rozhodne vyrazit vstříc nebezpečíí oceánu za
  účelem prolomení jeho neštěstí a ulovení ryby. Září je označováno jako měsíc
  velkých ryb. Santiago má velmi dobrý vztah s chlapcem jménem Manolin, kterého
  odjakřiva bere na výpravy s sebou. Pro starcovu smůlu Manolinovi rodiče
  přikážou jezdit s jiným člunem. Chlapec starci alespoň pomáhá se zajištěním
  výstroje potřebné pro výpravu.
- Časně z rána Santiago vyplouvá na moře. Orientuje se podle letu ptáků a snaží
  se směřovat k hejnům ryb. Během výpravy je stařec vystaven nepříjemnostem. Po
  několika dnech mimo civilizaci se cítí osaměle, začne mluvit sám se sebou o
  svých problémech, starostech a myšlenkách.
- Po několika dnech na volném moři stařec ucítí na jedné ze svých šnůr velký
  tlak. Je přesvědčen, že v hlubinách se mu na návnadu chytla velká ryba.
  Přestože je vyčerpán, je rozhodnutý všechny své zbývající síly vložit do
  zápasu s rybou, která ho táhne spolu s jeho lodí. Cítí se naprosto bezmocný.
- Kromě psychické únavy kompenzované samomluvou trpí i fyzickým vyčerpáním. Na
  mnoha místech těla je zraněný, má odřeniny, škrábance a chytají ho křeče do
  ruky, které mu komplikují souboj s rybou. Energii může získat jedině z masa
  ryb, které si sám uloví. Přestože není pobožný, odříkává modlitby. Sní nad
  tím, jaké by to bylo, kdyby s ním byl na lodi chlapec. Honí se mu hlavou
  spousta myšlenek a úvah. Jeho ohromná odhodlanost a odvaha mu dodávají sílu.
- V posledních dnech zápasí se žraloky. Při různých soubojích se mu stávají
  nepříjemnosti. Prva ztratí harpunu, poté nůž a pádlo. Po bitvě se žraloky se
  stařík vrací do vesnice zcela vyřízený s kostrou ryby, kterou ulovil. Chlapec
  se o něj stará a ostatní ho obdivují a říkají, že taková ryba tady ještě
  nebyla.

### Členění

- Souvislý text, odstavce

### Kompozice

- chronologická

### Téma

- Boj starce s rybou a sebou samotným

### Motivy

- lidská síla
- čestný boj
- odhodlanost
- láska
- odvaha
- samota
- beznaděj

### Postavy

- **Stařec Santiago**
  - Odvážný rybář s pevnou vůlí, samotář, bojuje až na pokraj svých sil, aby se
    uživil, introvert, prostý člověk, starý, silný, vytrvalý
- **Chlapec Manolin**
  - Obětavý, laskavý, nápomocný, má rád starce, pomáhá mu obdivuje ho

### Časoprostor

- Kubánská vesnice poblíž Havany, chatrč starce, loďka na moři
- 1940

### Vypravěcí způsoby

- er-forma

### Jazyk

- jednoduchá spisovná čeština
- španělské výrazy
- popisné části
- monology
- krátké věty
- rybářská pojmenování
- obecné označení
- metafory
- přirovnání
- personifikace
