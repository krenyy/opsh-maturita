> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/yqwdHo_Lf3w"></iframe>

### Autor

- [[John Steinbeck]]

### Literární

- **druh:**
  - epika
- **forma:**
  - sociálně kritická próza
- **žánr:**
  - novela s prvky balady
- **směr:**
  - americký realismus 1. poloviny 20. století, meziválečná literatura

### Obsah

- Hlavními hrdiny této novely jsou dva muži - dělníci. Duševně postižený Lennie
  Small byl vychováván svou tetou Klárou a po její smrti se ho ujal jeho kamarád
  George Milton. Stal se jeho ochráncem a snažil se vždy urovnat problémy, které
  Lennie občas nechtěně způsobil. Lennie měl rád hebké věci a rád se jich
  dotýkal. Často hladil myši a jiná zvířátka, ale protože měl velkou sílu, vždy
  je neúmyslně usmrtil. Lennie a George putovali společně za prací na farmu
  poblíž Soledadu. Zde se seznámili s dalšími dělníky, kteří tam pracovali. Oba
  muži snili o tom, že až vydělají dostatek peněz, pořídí si malé hospodářství.
  Lennie se těšil, že se bude starat o králíky s jemnou srstí. Později se svým
  nápadem seznámili starého uklízeče jménem Candy, který přišel o část ruky. Ten
  měl naspořené nějaké peníze, a tak se domluvili, že si časem společně koupí
  domek se zahradou. George tušil, že by Lennie mohl způsobit nějaké problémy.
  Už jednou totiž museli utéct z města, protože si Lennie chtěl pohladit šaty
  jedné dívky a ona ho obvinila ze znásilnění. Řekl mu proto, že kdyby se něco
  stalo, má se schovat ve vrbách poblíž řeky a on pak pro něj zajde... Syn
  majitele farmy Curley se rád pral a často vyvolával konflikty. Jednou napadl i
  Lennieho. Ten nejprve na jeho útok nereagoval, ale pak mu téměř rozdrtil ruku.
  Curley byl čerstvě ženatý, ale jeho žena s ním nebyla šťastná. Byla velmi
  krásná, a tak často koketovala s dělníky a snažila se upoutat jejich
  pozornost. Lennie dostal malé štěňátko, ale mazlil se s ním tak intenzivně, že
  ho nechtěně zabil. Pak za ním do stáje přišla Curleyova žena a vyprávěla mu o
  svém nepovedeném životě. Chtěla se stát herečkou, ale nakonec si musela vzít
  muže, kterého neměla ráda. Lennie jí vyprávěl o své zálibě v hebkých věcech, a
  tak mu dovolila, aby si pohladil její vlasy. Lennie byl jako u vytržení a
  stiskl její kadeře tak silně, že žena začala křičet. Lennie se polekal, zacpal
  jí pusu a zatřásl s ní tak silně, že jí zlomil vaz. Když viděl, co provedl,
  utekl se schovat k řece, jak mu to poradil George... Když Curley zjistil, co
  se stalo, rozhodl se Lennieho najít a zastřelit. George chtěl svého přítele
  ušetřit bolesti, a proto za ním šel k řece, kde mu vyprávěl o jejich společném
  snu, a když byl Lennie otočený zády, střelil ho do týla...

### Členění

-

### Kompozice

- chronologická, občas záblesky z minulosti

### Téma

- Celkové: autor se snaží zachytit důležitost onoho přátelství a důležitost
  lásky. Ukazuje, jak se může sen v mžiku rozpadlout
- Hlavní: přátelský vztah mezi Georgem a Lenniem
- Vedlejší: poukazuje na rasismus v tehdejší době, na odměřené vztahy bílých k
  černochům. Záliba Lennieho v měkkých věcech

### Motivy

- láska
- přátelství
- sociální nejistota sezonních dělníků
- rasismus
- touha po majetku
- hospodářská krize
- smrt
- vražda
- nehoda
- nevinnost
- nesplněné sny

### Postavy

- **George**
  - hodný, obětavý, toužící po spokojeném životě. I když se na Lennieho často
    zlobí a vyčítá mu, že se o něho musí starat, má ho ve skutečnosti velice
    rád.
- **Lennie**
  - působí dobráckým dojmem, je velmi pracovitý - "udělá, co se mu řekne". Je
    bohužel hodně naivní a hloupý, chová se jako malé dítě a ze spousty věcí
    nemá rozum. Mám svůj sen o šťastném životě, ale ten se bohužel nevyplní.
    Ačkoliv to asi není jeho záměr, chová se místy docela sobecky a také umí
    citově vydírat. To se ale asi dá přičíst jeho mentálnímu postižení.
- **Curley**
  - žárlivý, zakomplexovaný, agresivní mladý muž, snažící se vynutit si
    autoritu.
- **Curleyova žena**
  - krásná, vyzývavá, nepříliš inteligentní. Pod slupkou svůdné ženy se však
    skrývá osamělá bytost, které chybí láska a která musí žít život, o jaký
    nestojí.

### Časoprostor

- 30\. léta 20. století, Soledad, Kalifornie

### Vypravěcí způsoby

- er-forma

### Jazyk

- Spisovný jazyk
- Nespisovné nářečí
- Hovorový jazyk
- Expresivní výrazy
- Jednoduchá a přehledná stavba textu
- Přirovnání, zdrobněliny, obecná čj, metafora

### Syntax

-
