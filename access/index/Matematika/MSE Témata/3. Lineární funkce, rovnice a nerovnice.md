1. K dané lineární funkci určete funkci inverzní a sestrojte grafy obou funkcí:

   - $f:y=\dfrac{3x-1}{2}$ ^1

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   x &= \frac{3y-1}{2} \\
   >   2x &= 3y-1 \\
   >   2x+1 &= 3y \\
   >   f:y &= \frac{2x+1}{3}
   > \end{aligned}
   > $$
   >
   > ![[./plots/0301.png]]

   - $f:y=1-\dfrac{2x}{5}$ ^2

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned} x &= 1-\frac{2y}{5} \\\\ 5x &= 5-2y \\\\ 5x-5 &= -2y \\\\ f:y &=
   > \frac{5-5x}{2} \end{aligned}
   > $$
   >
   > ![[./plots/0302.png]]

2. Sestrojte grafy uvedených funkcí a určete jejich vlastnosti:

   - $f_1:y=1-2x$ ^3

   > [!done]- Řešení
   >
   > ![[./plots/0303.png]]
   >
   > - klesající
   > - $D\left(f\right) = \mathbb{R}$
   > - $H\left(f\right) = \mathbb{R}$

   - $f_2:y=|1-2x|$ ^4

   > [!done]- Řešení
   >
   > ![[./plots/0304.png]]
   >
   > - klesající - $\left(-\infty,\frac{1}{2}\right>$
   > - rostoucí - $\left<\frac{1}{2},\infty\right)$
   > - $D\left(f\right) = \mathbb{R}$
   > - $H\left(f\right) = \left<0,\infty\right)$

   - $f_3:y=1-2|x|$ ^5

   > [!done]- Řešení
   >
   > ![[./plots/0305.png]]
   >
   > - rostoucí - $\left(-\infty,0\right>$
   > - klesající - $\left<0,\infty\right)$
   > - $D\left(f\right) = \mathbb{R}$
   > - $H\left(f\right) = \left(-\infty,1\right>$

3. Užitím vhodné substituce řešte soustavu rovnic:

   - $\begin{aligned} \begin{cases} \dfrac{1}{x}+\dfrac{1}{y}+\dfrac{2}{z} &= -1 \\\\
     \dfrac{4}{x}+\dfrac{1}{y}+\dfrac{4}{z} &= -2 \\\\ -\dfrac{2}{x}+\dfrac{1}{y}-\dfrac{2}{z}
     &= 4 \end{cases} \end{aligned}$ ^6

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   a &= \frac{1}{x} \\
   >   b &= \frac{1}{y} \\
   >   c &= \frac{2}{z} \\
   >   \\
   >   \begin{cases}
   >     a+b+c &= -1 \\
   >     4a+b+2c &= -2 \\
   >     -2a+b-c &= 4
   >   \end{cases} \\
   >   \\
   >   \begin{cases}
   >     -2a-2b-2c &= 2 \\
   >     4a+b+2c &= -2 \\
   >     -2a+b-c &= 4
   >   \end{cases} \\
   >   \\
   >   -c &= 4 \\
   >   c &= -4 \\
   >   \\
   >   a+b+c &= -1 \\
   >   a+b-4 &= -1 \\
   >   a+b &= 3 \\
   >   a &= 3-b \\
   >   \\
   >   4a+b+2c &= -2 \\
   >   4\left(3-b\right)+b-8 &= -2 \\
   >   12-4b+b-8 &= -2 \\
   >   4-3b &= -2 \\
   >   -3b &= -6 \\
   >   b &= 2 \\
   >   \\
   >   a &= 3-b \\
   >   a &= 3-2 \\
   >   a &= 1 \\
   >   \\
   >   a &= \frac{1}{x} \\
   >   1 &= \frac{1}{x} \\
   >   x &= 1 \\
   >   \\
   >   b &= \frac{1}{y} \\
   >   2 &= \frac{1}{y} \\
   >   2y &= 1 \\
   >   y &= \frac{1}{2} \\
   >   \\
   >   c &= \frac{2}{z} \\
   >   -4 &= \frac{2}{z} \\
   >   -4z &= 2 \\
   >   z &= -\frac{1}{2}
   > \end{aligned}
   > $$

4. V oboru reálných čísel řešte nerovnice:

   - $\dfrac{2x+3}{x-1}\leq1$ ^7

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   \frac{2x+3}{x-1} &\leq 1 \\
   >   \\
   >   x &\neq 1 \\
   >   \\
   >   \frac{2x+3}{x-1}-\frac{x-1}{x-1} &\leq 0 \\
   >   \\
   >   \frac{2x+3-x+1}{x-1} &\leq 0 \\
   >   \\
   >   \frac{x+4}{x-1} &\leq 0 \\
   >   \\
   >   \frac{x+4}{x-1}\times\left(x-1\right)^{2} &\leq 0 \\
   >   \\
   >   \left(x+4\right)\left(x-1\right) &\leq 0 \\
   >   \\
   >   &\begin{cases} x+4 &\leq 0 \\
   >     x-1 &\geq 0
   >   \end{cases} \\
   >   &\begin{cases}
   >     x &\leq -4 \\
   >     x &\geq 1
   >   \end{cases} \\
   >   \\
   >   x_1 &\in \emptyset \\
   >   \\
   >   &\begin{cases}
   >     x+4 &\geq 0 \\
   >     x-1 &\leq 0
   >   \end{cases} \\
   >   &\begin{cases}
   >     x &\geq -4 \\
   >     x &\leq 1
   >   \end{cases} \\
   >   \\
   >   x_2 &\in \left<-4,1\right> \\
   >   \\
   >   x &\in \left<-4,1\right)
   > \end{aligned}
   > $$

   - $\dfrac{x+2}{x}>\dfrac{x+4}{x+2}$ ^8

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   \frac{x+2}{x} &> \frac{x+4}{x+2} \\
   >   \\
   >   x &\neq 0 \\
   >   x &\neq -2 \\
   >   \\
   >   \frac{x^{2}+4x+4-x^{2}-4x}{x^{2}+2x} &> 0 \\
   >   \\
   >   \frac{4}{x^{2}+2x} &> 0 \\
   >   \\
   >   4\left(x^{2}+2x\right) &> 0 \\
   >   \\
   >   4x\left(x+2\right) &> 0 \\
   >   \\
   >   &\begin{cases}
   >     4x &> 0 \\
   >     x+2 &> 0
   >   \end{cases} \\
   >   &\begin{cases}
   >     x &> 0 \\
   >     x &> -2
   >   \end{cases} \\
   >   \\
   >   x_1 &\in \left(0,\infty\right) \\
   >   \\
   >   &\begin{cases}
   >     4x &< 0 \\
   >     x+2 &< 0
   >   \end{cases} \\
   >   &\begin{cases}
   >     x &< 0 \\
   >     x &< -2
   >   \end{cases} \\
   >   \\
   >   x_2 &\in \left(-\infty,-2\right) \\
   >   \\
   >   x &\in \left(-\infty,-2\right) \cup \left(0,\infty\right)
   > \end{aligned}
   > $$

5. Řešte rovnici a proveďte zkoušku:

   - $|x+2|+|x-3|=1+2x$ ^9

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   |x+2|+|x-3| &= 1+2x \\
   >   \\
   >   |x+2|+|x-3|-2x &= 1 \\
   >   \\
   >   &\begin{cases}
   >     &\begin{cases}
   >       {\color{green}x+2}\ {\color{green}+\ x-3}-2x &= 1 \\
   >       x+2 &{\color{green}\geq} \ 0 \\
   >       x-3 &{\color{green}\geq} \ 0
   >     \end{cases} \\
   >     &\begin{cases}
   >       {\color{green}x+2}\ {\color{red}-\ x+3}-2x &= 1 \\
   >       x+2 &{\color{green}\geq} \ 0 \\
   >       x-3 &{\color{red}<} \ 0
   >     \end{cases} \\
   >     &\begin{cases}
   >       {\color{red}-x-2} \ {\color{green}+\ x-3}-2x &= 1 \\
   >       x+2 &{\color{red}<} \ 0 \\
   >       x-3 &{\color{green}\geq} \ 0
   >     \end{cases} \\
   >     &\begin{cases}
   >       {\color{red}-x-2} \ {\color{red}-\ x+3}-2x &= 1 \\
   >       x+2 &{\color{red}<} \ 0 \\
   >       x-3 &{\color{red}<} \ 0
   >     \end{cases} \\
   >   \end{cases} \\
   >   \\
   >   &\begin{cases}
   >     &\begin{cases}
   >       -1 &= 1 \\
   >       x &\geq -2 \\
   >       x &\geq 3
   >     \end{cases} \\
   >     &\begin{cases}
   >       x &= 2 \\
   >       x &\geq -2 \\
   >       x &< 3
   >     \end{cases} \\
   >     &\begin{cases}
   >       x &= -3 \\
   >       x &< -2 \\
   >       x &\geq 3
   >     \end{cases} \\
   >     &\begin{cases}
   >       x &= 0 \\
   >       x &< -2 \\
   >       x &< 3
   >     \end{cases} \\
   >     \\
   >     &\begin{cases}
   >       x &\in \emptyset \\
   >       x &= 2 \\
   >       x &\in \emptyset \\
   >       x &\in \emptyset
   >     \end{cases}
   >   \end{cases} \\
   >   \\
   >   x &= 2
   > \end{aligned}
   > $$

6. V oboru celých čísel řešte nerovnici:

   - $|5x-2|+2x\leq6-x$ ^10

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   |5x-2|+3x &\leq 6 \\
   >   \\
   >   &\begin{cases}
   >     &\begin{cases}
   >       {\color{green}5x-2}+3x &\leq 6 \\
   >       5x-2 &{\color{green}\geq} \ 0
   >     \end{cases} \\
   >     &\begin{cases}
   >       {\color{red}-5x+2}+3x &\leq 6 \\
   >       5x-2 &{\color{red}<} \ 0
   >     \end{cases}
   >   \end{cases} \\
   >   \\
   >   &\begin{cases}
   >     &\begin{cases}
   >       x &\leq 1 \\
   >       x &\geq \frac{2}{5}
   >     \end{cases} \\
   >     &\begin{cases}
   >       x &\geq -2 \\
   >       x &< \frac{2}{5}
   >     \end{cases}
   >   \end{cases} \\
   >   \\
   >   &\begin{cases}
   >     x &\in \left<\frac{2}{5},1\right> \\
   >     x &\in \left<-2,\frac{2}{5}\right)
   >   \end{cases} \\
   >   \\
   >   x &\in \left<-2,1\right>
   > \end{aligned}
   > $$
