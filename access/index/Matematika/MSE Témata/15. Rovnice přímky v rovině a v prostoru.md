1. Jsou dány body $A\left[−1,3\right]$, $B\left[2,1\right]$, $C\left[c_x,−2\right]$.

   - Napište parametrickou a obecnou rovnici přímky $p=\overleftrightarrow{\rm AB}$.

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   \vec{u} &= \left(3,-2\right) \\
   >   X &= \left(-1+3s,3-2s\right) \\
   >   \\
   >   \vec{n} &= \left(2,3\right) \\
   >   2x+3y+c &= 0 \\
   >   2\left(-1\right)+3\left(3\right)+c &= 0 \\
   >   -2+9+c &= 0 \\
   >   7+c &= 0 \\
   >   c &= -7 \\
   >   2x+3y-7 &= 0
   > \end{aligned}
   > $$

   - Určete její směrnici a souřadnici $c_x$ tak, aby bod $C$ ležel na této přímce.

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   2x+3y-7 &= 0 \\
   >   2x+3y &= 7 \\
   >   3y &= -2x+7 \\
   >   y &= -\frac{2x}{3}+\frac{7}{3} \\
   >   \\
   >   y &= 3-2s \\
   >   -2 &= 3-2s \\
   >   -5 &= -2s \\
   >   5 &= 2s \\
   >   s &= 2.5 \\
   >   c_x &= -1+3s \\
   >   c_x &= -1+3\left(2.5\right) \\
   >   c_x &= -1+7.5 \\
   >   c_x &= 6.5
   > \end{aligned}
   > $$

2. $p=\left\{\left[1−2t;−1+6t\right], t\in\mathbb{R}\right\}$, $q:x-y−6=0$, $r:2x-y+4=0$

   - Určete průsečík $P$ přímek $p$, $q$.

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   x-y-6 &= 0 \\
   >   \left(1-2t\right)-\left(-1+6t\right)-6 &= 0 \\
   >   1-2t+1-6t-6 &= 0 \\
   >   -8t-4 &= 0 \\
   >   8t &= -4 \\
   >   t &= -0.5 \\
   >   P &= \left[1-2\left(-0.5\right),-1+6\left(-0.5\right)\right] \\
   >   P &= \left[2,-4\right] \\
   > \end{aligned}
   > $$

   - $P$ veďte kolmici na $r$.

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   > \vec{n_r} &= \left(2,-1\right) \\
   > X &= \left(2+2s,-4-s\right)
   > \end{aligned}
   > $$

   - $P$ veďte rovnoběžku na $r$.

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   \vec{u_r} &= \left(1,2\right) \\
   >   X &= \left(2+s,-4+2s\right)
   > \end{aligned}
   > $$

3. Body $X\left[−2,−3\right]$, $Y\left[6,−1\right]$, $Z\left[1,3\right]$ tvoří trojúhelník.

   - Určete souřadnice bodu $V$, který je průsečíkem výšek $\triangle XYZ$.

   > [!todo]- Řešení

4. Je dán $\triangle ABC$: $A\left[−5,8,1\right]$, $B\left[3,−1,0\right]$, $C\left[−1,5,−4\right]$.

   - Napište rovnici strany $b$ a těžnice $t_a$.

   > [!todo]- Řešení

   - Užitím rovnice těžnice $t_a$ určete souřadnice těžiště $\triangle$.

   > [!todo]- Řešení

5. $p=\left\{\left[3−2t;−1+t;2+4t\right], t\in\mathbb{R}\right\}$

   - Vypočítejte souřadnice bodů ve kterých přímka protíná souřadnicové roviny $\rho_{xy}$, $\rho_{xz}$, $\rho_{yz}$.

   > [!todo]- Řešení

6. Určete vzájemnou polohu přímek $p$, $q$:

   - $p=\left\{\left[−6+t;7−t;2t\right], t\in\mathbb{R}\right\}$
   - $q=\left\{\left[−5−s;3−2s;5+s\right], s\in\mathbb{R}\right\}$

   > [!todo]- Řešení

   - $p: A\left[2,4,1\right]$, $\overrightarrow{\rm u}=\left(0,−1,2\right)$
   - $q: B\left[1,2,−1\right]$, $\overrightarrow{\rm v}=\left(−1,3,−2\right)$

   > [!todo]- Řešení
